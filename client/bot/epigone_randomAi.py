#!/usr/bin/env python3
import requests
import sys
import random

size = 19
logFile = open("logFile.txt","w+")

def play():
    logFile.write(" < " + "PUT " + str(random.randrange(size * size)) + "\n")
    return ("PUT " + str(random.randrange(size * size)))

def play_pass():
    logFile.write(" < " + "PASS" + "\n")
    return ("PASS")

def info(entry):
    data = entry.split()
    if data[1] == "SIZE":
        size = int(data[2])

def main(args):
    while 1:
        entry = input()
        logFile.write(" > " + entry + "\n")
        data = entry.split()
        if data[0] == "START":
            print(play())
        if data[0] == "INFO":
            info(entry);
        if data[0] == "PUT":
            print(play());
        if data[0] == "PASS":
            print(play_pass());
        if data[0] == "STOP":
            logFile.close();
            exit(0);


if __name__ == '__main__':
     main(sys.argv)
