const path = require('path');
const electron = require('electron');
const request = require('request');

const joinButtonFct = (value, connected) => {
    if (!connected) {
        request.get('http://' + electron.remote.getGlobal('sharedObject').serverIp + ':' + electron.remote.getGlobal('sharedObject').serverPort + '/room/join', {
                headers: {usertoken: electron.remote.getGlobal('sharedObject').userToken, roomname: value}
            },
            function (err, aze, ret) {
                if (err) {
                    document.getElementById('ErrorNotification').innerText = "Can't connect to the server";
                    document.getElementById('ErrorNotificationContainer').style.display = "block";
                } else {
                    try {
                        if (ret === "OK") {
                            electron.remote.getGlobal('sharedObject').roomName = value;
                            document.location.replace(path.join(__dirname, 'res/room.html'));
                        } else {
                            document.getElementById('ErrorNotification').innerText = JSON.parse(ret).error.message;
                            document.getElementById('ErrorNotificationContainer').style.display = "block";
                        }
                    } catch (e) {
                        console.log("ERROR : " + e);
                    }
                }
            });
    } else {
        electron.remote.getGlobal('sharedObject').roomName = value;
        document.location.replace(path.join(__dirname, 'res/room.html'));
    }
};

module.exports = joinButtonFct;
