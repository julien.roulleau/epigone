const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');

let mainWindow;

function createWindow () {
    console.log("Create Window");
    mainWindow = new BrowserWindow({width: 1400, height: 800});

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'res/log.html'),
        protocol: 'file:',
        slashes: true
    }));

    mainWindow.on('closed', function () {
        mainWindow = null
    });
}

app.on('ready', () => {
    createWindow();
    global.sharedObject = {
        userToken: '',
        isConnected: false,
        roomName: '',
        pseudonyme: '',
        serverPort: '8080',
        serverIp: '127.0.0.1',
        roomList: [],
        myRoomList: [],
        sockets: {}
    };
});

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        try {
            if (global.sharedObject.userToken) {
                request.get('http://localhost:8080/logout', {
                    headers: {usertoken: global.sharedObject.userToken}
                });
            }
        } catch (ignored) {}
        app.quit()
    }
});

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow()
    }
});

