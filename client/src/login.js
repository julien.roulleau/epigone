const request = require("request");
const path = require('path');
const url = require('url');
const electron = require('electron');

const loginButton = document.getElementById('login_button');

loginButton.addEventListener("click", function (event) {
    if (document.getElementById('server_ip').value === '' || document.getElementById('server_port').value === 'undefined') {
        document.getElementById('ErrorNotificationContainer').style.display = "block";
        document.getElementById('ErrorNotification').innerText = "IP and Port must be set";
        return;
    }
    if (document.getElementById('pseudonyme').value === '') {
        electron.remote.getGlobal('sharedObject').serverIp = document.getElementById('server_ip').value;
        electron.remote.getGlobal('sharedObject').serverPort = document.getElementById('server_port').value;
        electron.remote.getGlobal('sharedObject').isConnected = false;
        document.location.replace(path.join(__dirname, 'res/home.html'));
        return;
    }
    request.get(
        'http://' + document.getElementById('server_ip').value + ':' + document.getElementById('server_port').value + '/login/login',
        {headers: {username: document.getElementById('pseudonyme').value}},
        function (err, aze, qsd) {
            if (err) {
                document.getElementById('ErrorNotification').innerText = "Can't connect to the server";
                document.getElementById('ErrorNotificationContainer').style.display = "block";
            }
            else {
                const resp = JSON.parse(qsd);
                if (resp.hasOwnProperty("data")) {
                    electron.remote.getGlobal('sharedObject').userToken = resp.data.usertoken;
                    electron.remote.getGlobal('sharedObject').pseudonyme = document.getElementById('pseudonyme').value;
                    electron.remote.getGlobal('sharedObject').serverIp = document.getElementById('server_ip').value;
                    electron.remote.getGlobal('sharedObject').serverPort = document.getElementById('server_port').value;
                    electron.remote.getGlobal('sharedObject').isConnected = true;
                    document.location.replace(path.join(__dirname, 'res/home.html'));
                } else {
                    document.getElementById('ErrorNotificationContainer').style.display = "block";
                    if (resp.hasOwnProperty("error")) {
                        console.log(resp.error.message);
                        document.getElementById('ErrorNotification').innerText = resp.error.message;
                    }
                }
            }
        });
});
