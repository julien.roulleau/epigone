const path = require("path");
const electron = require('electron');

const joinSpectatorButtonFct = (value) => {
    try {
        electron.remote.getGlobal('sharedObject').roomName = value;
        document.location.replace(path.join(__dirname, 'res/room.html'));
    } catch (e) {
        console.log("ERROR : " + e);
    }
};

module.exports = joinSpectatorButtonFct;
