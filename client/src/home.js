const electron = require('electron');
const request = require("request");
const path = require("path");

function addMyRoom(roomName, roomSize) {
    if (roomName === "")
        return "";
    const container = document.getElementById('ListMyRoomContainer');

    let newRoomDiv = document.createElement('div');
    newRoomDiv.setAttribute('id', 'RoomContainer' + roomName);
    newRoomDiv.setAttribute('class', 'RoomContainer');

    let roomNameP = document.createElement('p');
    roomNameP.innerText = "Name : " + roomName;

    let roomSizeP = document.createElement('p');
    roomSizeP.innerText = "Board size : " + roomSize;

    let roomJoinButton = document.createElement('button');
    if (electron.remote.getGlobal('sharedObject').isConnected) {
        roomJoinButton.setAttribute('id', 'RoomJoin' + roomName);
        roomJoinButton.setAttribute('class', 'RoomJoinButton');
        roomJoinButton.setAttribute('onclick', "joinButtonFct(value, true);");
        roomJoinButton.setAttribute('value', roomName);
        roomJoinButton.innerText = "Join";
    }

    newRoomDiv.appendChild(roomNameP);
    newRoomDiv.appendChild(roomSizeP);
    if (electron.remote.getGlobal('sharedObject').isConnected)
        newRoomDiv.appendChild(roomJoinButton);
    container.appendChild(newRoomDiv);
}

function addRoom(roomName, roomSize, canJoin) {
    if (roomName === "")
        return "";
    const container = document.getElementById('ListRoomContainer');

    let newRoomDiv = document.createElement('div');
    newRoomDiv.setAttribute('id', 'RoomContainer' + roomName);
    newRoomDiv.setAttribute('class', 'RoomContainer');

    let roomNameP = document.createElement('p');
    roomNameP.innerText = "Name : " + roomName;

    let roomSizeP = document.createElement('p');
    roomSizeP.innerText = "Board size : " + roomSize;

    let roomJoinButton = document.createElement('button');
    if (canJoin && electron.remote.getGlobal('sharedObject').isConnected) {
        roomJoinButton.setAttribute('id', 'RoomJoin' + roomName);
        roomJoinButton.setAttribute('class', 'RoomJoinButton');
        roomJoinButton.setAttribute('onclick', "joinButtonFct(value, false);");
        roomJoinButton.setAttribute('value', roomName);
        roomJoinButton.innerText = "Join as player";
    }

    let roomJoinSpectatorButton = document.createElement('button');
    roomJoinSpectatorButton.setAttribute('id', 'RoomJoinSpectator' + roomName);
    roomJoinSpectatorButton.setAttribute('class', 'RoomJoinSpectatorButton');
    roomJoinSpectatorButton.setAttribute('onclick', "joinSpectatorButtonFct(value);");
    roomJoinSpectatorButton.setAttribute('value', roomName);
    roomJoinSpectatorButton.innerText = "Join as spectator";


    newRoomDiv.appendChild(roomNameP);
    newRoomDiv.appendChild(roomSizeP);
    if (canJoin && electron.remote.getGlobal('sharedObject').isConnected)
        newRoomDiv.appendChild(roomJoinButton);
    newRoomDiv.appendChild(roomJoinSpectatorButton);
    container.appendChild(newRoomDiv);
}

request.get('http://' + electron.remote.getGlobal('sharedObject').serverIp + ':' + electron.remote.getGlobal('sharedObject').serverPort + '/room/list', {
    headers: {}},
    function (err, aze, ret) {
        if (err) {
            document.getElementById('ErrorNotification').innerText = "Can't connect to the server";
            document.getElementById('ErrorNotificationContainer').style.display = "block";
        } else {
            try {
                console.log(ret);
                const json = JSON.parse(ret);
                electron.remote.getGlobal('sharedObject').roomList = json.data.games;
                json.data.games.forEach(function (value, index, array) {
                    console.log(value);
                    if (electron.remote.getGlobal('sharedObject').isConnected &&
                        (value.white === electron.remote.getGlobal('sharedObject').pseudonyme || value.black === electron.remote.getGlobal('sharedObject').pseudonyme)) {
                        addMyRoom(value.name, value.board.size);
                    } else
                        addRoom(value.name, value.board.size, value.canJoin);
                })
            } catch (e) {
                console.log("ERROR : " + e);
            }
        }
});

const createRoomButton = document.getElementById('createRoomButton');
createRoomButton.addEventListener('click', function (event) {
    const size = document.getElementById('createRoomSize').value;
    const name = document.getElementById('createRoomName').value;
    const time = document.getElementById('createRoomTimeout').value;

    if (name === "" || size === "" || time === "") {
        document.getElementById('ErrorNotificationContainer').style.display = "block";
        document.getElementById('ErrorNotification').innerText = "Parameters are incorrect";
        return;
    }
    request.get('http://' + electron.remote.getGlobal('sharedObject').serverIp + ':' + electron.remote.getGlobal('sharedObject').serverPort + '/room/create', {
        headers: {
            usertoken: electron.remote.getGlobal('sharedObject').userToken,
            roomname: name,
            size: size,
            timeout: time
        }
    }, function (err, aze, ret) {
        if (err) {
            document.getElementById('ErrorNotification').innerText = "Can't connect to the server";
            document.getElementById('ErrorNotificationContainer').style.display = "block";
        } else {
            if (ret === "OK") {
                electron.remote.getGlobal('sharedObject').roomName = name;
                document.location.replace(path.join(__dirname, 'res/room.html'));
            } else {
                const resp = JSON.parse(ret);
                document.getElementById('ErrorNotificationContainer').style.display = "block";
                document.getElementById('ErrorNotification').innerText = resp.error.message;
            }
        }
    });
});

document.getElementById('backButton').addEventListener('click', function (event) {
    document.location.replace(path.join(__dirname, 'res/log.html'));
});

if (!electron.remote.getGlobal('sharedObject').isConnected) {
    document.getElementById('CreateRoomContainer').style.display = "none";
} else {
    document.getElementById('backButton').style.display = "none";
}
