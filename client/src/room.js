const electron = require('electron');
const request = require("request");
const path = require("path");
const io = require("socket.io-client");
const {spawn} = require('child_process');

/*
 * ------------------------------
 * Game variable declaration
 * ------------------------------
 */
let g_boardSize = 19;
let g_board = [];
let g_territory = [];
let g_currentPiece = -1;
let g_personalColor = 'black';
let g_isPlayer = true;
let g_roomName = '';
let g_turn = '';
let g_ia = false;
let g_white = '';
let g_black = '';
let g_isConnected = false;
let g_history = [];
let g_current_history = 0;
let g_history_current_board = [];
let g_history_init = true;
let g_history_last = true;
let g_timeout = 0;
let g_time = 0;
let g_interval = setInterval(() => {}, 1000);

/*
 * ------------------------------
 * Document Elements declaration
 * ------------------------------
 */
const putButton = document.getElementById('putStone');
const passButton = document.getElementById('passTurn');
const board = document.getElementById('board');
const turnImg = document.getElementById('turnImg');
const infoImg = document.getElementById('infoImg');
const personnaleInfoContainer = document.getElementById('personnaleInfoContainer');
const iaCheckBox = document.getElementById('iaCheckBox');
const iaSelectionButton = document.getElementById('iaSelectionButton');
const gameActionContainer = document.getElementById('gameAction');
const winnerContainer = document.getElementById('winnerContainer');
const winnerMessage = document.getElementById('winnerMessage');
const iaContainer = document.getElementById('iaContainer');
const turnContainer = document.getElementById('turnContainer');
const backButton = document.getElementById('backButton');
const sendMessageButton = document.getElementById("sendMessageButton");
const newMessageInput = document.getElementById("newMessageInput");
const chatMessagesContainer = document.getElementById("chatMessagesContainer");
const newMessageContainer = document.getElementById("newMessageContainer");
const activateTerritoryCheckBox = document.getElementById("activateTerritoryCheckBox");
const territoryContainer = document.getElementById('territoryContainer');
const historyCurrentActionContainer = document.getElementById('historyCurrentActionContainer');
const historyCurrentColor = document.getElementById('historyCurrentColor');
const historyCurrentAction = document.getElementById('historyCurrentAction');
const historyFirst = document.getElementById('historyFirst');
const historyPrev = document.getElementById('historyPrev');
const historyNext = document.getElementById('historyNext');
const historyLast = document.getElementById('historyLast');
const historyIndex = document.getElementById('historyIndex');

/*
 * ------------------------------
 * Stone interaction function
 * ------------------------------
 */
function remove_stones() {
    let stoneContainer = document.getElementById("stoneContainer");
    while (stoneContainer.firstChild) {
        stoneContainer.removeChild(stoneContainer.firstChild);
    }
}

function place_stone(color, x, y) {
    const stoneContainer = document.getElementById('stoneContainer');

    const boardSize = g_boardSize;
    let stoneImg = document.createElement('img');
    const stoneSize = 100 / (parseInt(boardSize) + 1);
    const decalage = parseInt(stoneSize) / 2;

    stoneImg.setAttribute('class', 'stone');
    if (color === 'black')
        stoneImg.setAttribute('src', '../../assets/piece_go_noire.png');
    else {
        stoneImg.setAttribute('src', '../../assets/piece_go_blanche.png');
    }
    stoneImg.setAttribute('style', 'margin-top: ' + (decalage + y * stoneSize) + '%; margin-left: ' + (decalage + x * stoneSize) + '%; width: ' + stoneSize + '%');
    stoneContainer.appendChild(stoneImg);
}

function replace_stones(board) {
    remove_stones();
    const size = g_boardSize;

    board.forEach((value, index, array) => {
        if (value === 1)
            place_stone('black', index % size, Math.trunc(index / size));
        else if (value === 2)
            place_stone('white', index % size, Math.trunc(index / size));

    });
    if (g_currentPiece !== -1) {
        let pos = parseInt(g_currentPiece);
        place_stone(g_personalColor, pos % size, Math.trunc(pos / size));
    }
}

/*
 * ------------------------------
 * Territory interaction function
 * ------------------------------
 */
function remove_territorys() {
    let territoryContainer = document.getElementById("territoryContainer");
    while (territoryContainer.firstChild) {
        territoryContainer.removeChild(territoryContainer.firstChild);
    }
}

function place_territory(color, x, y) {
    const boardSize = g_boardSize;
    let territoryImg = document.createElement('img');
    const stoneSize = (100 / (parseInt(boardSize) + 1));
    const territorySize = (100 / (parseInt(boardSize) + 1)) / 2;
    const decalage = parseInt(stoneSize) / 2 + parseInt(territorySize) / 2 + 0.2;

    territoryImg.setAttribute('class', 'territory');
    if (color === 'black')
        territoryImg.setAttribute('src', '../../assets/territory_black.png');
    else {
        territoryImg.setAttribute('src', '../../assets/territory_white.png');
    }
    territoryImg.setAttribute('style', 'margin-top: ' + (decalage + y * stoneSize) + '%; margin-left: ' + (decalage + x * stoneSize) + '%; width: ' + territorySize + '%');
    territoryContainer.appendChild(territoryImg);
}

function replace_territorys() {
    remove_territorys();
    const t_board = g_territory;
    const size = g_boardSize;

    t_board.forEach((value, index, array) => {
        if (value === 11 || value === 112)
            place_territory('black', index % size, Math.trunc(index / size));
        else if (value === 12 || value === 111)
            place_territory('white', index % size, Math.trunc(index / size));

    });
}


/*
 * ------------------------------
 *  IA Communication
 * ------------------------------
 */

function Bot() {
    self = this;
    g_ia = false;
}

Bot.prototype.launch = function (path) {
    g_ia = true;
    this.bot = spawn(path);
    this.bot.on('error', (err) => {
        console.log('Failed to start subprocess. ' + err);
    });
    this.bot.stdout.on('data', (data) => this.receive(data));
    this.infoIdentity();
    this.infoSize();
    this.infBoard();
};

Bot.prototype.stop = function () {
    g_ia = false;
    this.bot.kill('SIGINT');
};

Bot.prototype.receive = function (data) {
    let cmd = data.toString().split(' ');
    if (cmd[0] === "PUT") {
        gameSocket.emit("put", {
            usertoken: electron.remote.getGlobal('sharedObject').userToken,
            pos: parseInt(cmd[1], 10)
        });
    } else if (cmd[0] === "PASS") {
        gameSocket.emit("pass", {usertoken: electron.remote.getGlobal('sharedObject').userToken});
    }
};

Bot.prototype.put = function (data) {
    let add = data.add;
    let remove = "";
    if (data.remove !== undefined)
        data.remove.forEach((elem, index) => {
            if (index === 0)
                remove += " " + elem;
            else
                remove += "," + elem;
        });
    this.bot.stdin.write("PUT " + add + remove + "\n");
};

Bot.prototype.pass = function () {
    this.bot.stdin.write("PASS\n");
};

Bot.prototype.init = function () {
    this.bot.stdin.write("START\n");
};

Bot.prototype.error = function () {
    this.bot.stdin.write("ERROR\n");
};

Bot.prototype.infoSize = function () {
    this.bot.stdin.write("INFO SIZE " + g_boardSize + "\n");
};

Bot.prototype.infBoard = function () {
    const board = g_board;
    let boardtxt = "";
    board.forEach((value, index) => {
        if (index === 0)
            boardtxt += value;
        else
            boardtxt += "," + value;
    });
    this.bot.stdin.write("INFO BOARD " + boardtxt + "\n");
};

Bot.prototype.infoIdentity = function () {
    this.bot.stdin.write("INFO ID " + (g_personalColor === 'black' ? 1 : 2) + "\n");
};

let bot = new Bot();

/*
 * ------------------------------
 * History Functions
 * ------------------------------
 */

function init_history() {
    g_history_init = false;
    g_history_last = true;
    g_current_history = g_history.length - 1;
    if (g_history.length === 0)
        g_current_history = 0;
}

function build_history_board() {
    g_history_current_board = [];
    g_board.forEach((value, index) => {
        g_history_current_board[index] = 0;
    });
    g_history.forEach((value, index) => {
        if (index <= g_current_history) {
            if (value.type !== "PASS") {
                g_history_current_board[value.add] = g_white === value.user ? 2 : 1;
                value.remove.forEach((value, index) => {
                    g_history_current_board[index] = 0;
                });
            }
        }
    })
}

function refresh_history() {
    historyIndex.innerText = "" + g_current_history;

    historyLast.disabled = false;
    historyNext.disabled = false;
    historyFirst.disabled = false;
    historyPrev.disabled = false;

    if (g_current_history === 0) {
        historyFirst.disabled = true;
        historyPrev.disabled = true;
    }
    if (g_current_history >= g_history.length - 1) {
        g_history_last = true;
        historyLast.disabled = true;
        historyNext.disabled = true;
        replace_stones(g_board);
    } else {
        g_history_last = false;
        territoryContainer.style.display = "none";
        activateTerritoryCheckBox.checked = false;
        g_currentPiece = -1;
        build_history_board();
        replace_stones(g_history_current_board);
    }

    if (g_history.length !== 0) {
        historyCurrentActionContainer.style.display = "block";
        if (g_white === (g_history[g_current_history]).user)
            historyCurrentColor.setAttribute('src', '../../assets/piece_go_blanche.png');
        else if (g_black === (g_history[g_current_history]).user)
            historyCurrentColor.setAttribute('src', '../../assets/piece_go_noire.png');
        historyCurrentAction.innerText = g_history[g_current_history].type;
    } else
        historyCurrentActionContainer.style.display = "none";
}

/*
 * ------------------------------
 * Timeout
 * ------------------------------
 */

function timeout_handler() {
    g_time--;
    document.getElementById('timeout').innerText = "" + g_time;
    if (g_time === 0) {
        clearInterval(g_interval);
    }
}

function reset_chrono() {
    g_time = g_timeout;
    if (g_timeout !== 0 && g_white !== "") {
        document.getElementById('timeout').innerText = "" + g_time;
        document.getElementById("timeoutContainer").style.display = "block";
        clearInterval(g_interval);
        g_interval = setInterval(timeout_handler, 1000);
    }
}

/*
 * ------------------------------
 * Socket communication
 * ------------------------------
 */
g_roomName = electron.remote.getGlobal('sharedObject').roomName;
const gameSocket = io.connect("http://" + electron.remote.getGlobal('sharedObject').serverIp + ":" + electron.remote.getGlobal('sharedObject').serverPort + "/" + g_roomName);

gameSocket.on("getBoard", function (data) {
    gameSocket.emit("getTerritoryBoard");
    gameSocket.emit("getHistory");
    g_board = data.data.board.board;
    g_currentPiece = -1;
    replace_stones(g_board);
});

gameSocket.on("put", function (data) {
    try {
        if (data.error !== undefined) {
            document.getElementById('notificationMessage').innerText = data.error.message;
            document.getElementById('notificationContainer').style.display = "block";
            if (g_ia) {
                bot.error();
            }
        } else {
            if (g_current_history === g_history.length - 1)
                g_current_history = g_current_history + 1;
            gameSocket.emit("getInfo");
            gameSocket.emit("getBoard");
            reset_chrono();
            document.getElementById('notificationContainer').style.display = "none";
            if (g_ia && data.data.user !== electron.remote.getGlobal('sharedObject').pseudonyme) {
                bot.put(data.data);
            }
        }
    } catch (ignored) {
        console.log(ignored);
    }
});

gameSocket.on("pass", function (data) {
    try {
        if (data.error !== undefined) {
            document.getElementById('notificationMessage').innerText = data.error.message;
            document.getElementById('notificationContainer').style.display = "block";
            if (g_ia) {
                bot.error();
            }
        } else {
            if (g_current_history === g_history.length - 1)
                g_current_history = g_current_history + 1;
            gameSocket.emit("getInfo");
            gameSocket.emit("getBoard");
            reset_chrono();
            document.getElementById('notificationContainer').style.display = "none";
            if (g_ia && data.user !== electron.remote.getGlobal('sharedObject').pseudonyme) {
                bot.pass();
            }
        }
    } catch (ignored) {
    }
});

gameSocket.on("getInfo", function (data) {
    g_turn = data.data.turn;
    if (data.data.isFinish) {
        winnerMessage.innerText = data.data.winner.username + " is the WINNER !";
        winnerContainer.style.display = "inline";
        iaContainer.style.display = "none";
        gameActionContainer.style.display = "none";
        turnContainer.style.display = "none";
    }
    g_boardSize = data.data.boardSize;
    g_isPlayer = true;
    if (g_isConnected && data.data.black === electron.remote.getGlobal('sharedObject').pseudonyme) {
        g_personalColor = 'black';
        infoImg.setAttribute('src', '../../assets/piece_go_noire.png');
    } else if (g_isConnected && data.data.white === electron.remote.getGlobal('sharedObject').pseudonyme) {
        g_personalColor = 'white';
        infoImg.setAttribute('src', '../../assets/piece_go_blanche.png');
    } else {
        g_isPlayer = false;
        g_personalColor = '';
        putButton.style.display = "none";
        passButton.style.display = "none";
        personnaleInfoContainer.style.display = "none";
        iaContainer.style.display = "none";
        newMessageContainer.style.dispay = "none";
    }
    g_black = data.data.black;
    g_timeout = data.data.timeout / 1000;
    if (g_timeout === 0)
        document.getElementById("timeoutContainer").style.display = "none";
    if (data.data.white !== undefined && data.data.timeout === 0)
        reset_chrono();
    if (data.data.white !== undefined)
        g_white = data.data.white;
    if (g_turn === undefined)
        turnImg.setAttribute('src', '../../assets/piece_go_blanche.png');
    else if (g_white === g_turn)
        turnImg.setAttribute('src', '../../assets/piece_go_blanche.png');
    else if (g_black === g_turn)
        turnImg.setAttribute('src', '../../assets/piece_go_noire.png');
    if (data.data.white !== undefined)
        document.getElementById("whitePseudo").innerText = data.data.white;
    document.getElementById("blackPseudo").innerText = data.data.black;
});

gameSocket.on("end", function (data) {
    winnerMessage.innerText = data.data.winner + " is the WINNER !";
    winnerContainer.style.display = "inline";
    iaContainer.style.display = "none";
    gameActionContainer.style.display = "none";
    turnContainer.style.display = "none";
    gameSocket.emit("getBoard");
});

gameSocket.on("userJoin", function (data) {
    let messageDiv = document.createElement("div");
    messageDiv.setAttribute('class', 'chatMessage');
    let authorLabel = document.createElement("label");
    authorLabel.setAttribute('class', 'authorMessage');
    let message = document.createElement("p");
    message.setAttribute('class', 'message');
    message.innerText = data.data.user + " join the room.";

    messageDiv.appendChild(authorLabel);
    messageDiv.appendChild(message);

    chatMessagesContainer.appendChild(messageDiv);
    gameSocket.emit("getInfo");
});

gameSocket.on("getHistory", function (data) {
    g_history = data.data;
    if (g_history_init)
        init_history();
    refresh_history()
});

gameSocket.on("getTerritoryBoard", function (data) {
    g_territory = data.data.territoryBoard;
    replace_territorys();
});

gameSocket.on("message", function (data) {
    console.log(data);
    let messageDiv = document.createElement("div");
    messageDiv.setAttribute('class', 'chatMessage');
    let authorLabel = document.createElement("label");
    authorLabel.setAttribute('class', 'authorMessage');
    authorLabel.innerText = data.data.user;
    let message = document.createElement("p");
    message.setAttribute('class', 'message');
    message.innerText = data.data.msg;

    messageDiv.appendChild(authorLabel);
    messageDiv.appendChild(message);

    chatMessagesContainer.appendChild(messageDiv);
});

/*
 * ------------------------------
 * Event Listener
 * ------------------------------
 */
board.addEventListener('click', function (event) {
    if (!g_isConnected || !g_isPlayer || g_turn !== electron.remote.getGlobal('sharedObject').pseudonyme || !g_history_last)
        return;
    let boardSize = parseInt(g_boardSize);
    let stoneSizeX = board.offsetWidth / (boardSize + 1);
    let posX = (event.offsetX - (stoneSizeX / 2)) / stoneSizeX;
    let stoneSizeY = board.offsetHeight / (boardSize + 1);
    let posY = (event.offsetY - (stoneSizeY / 2)) / stoneSizeY;
    if (posX > 0 && posX < boardSize && posY > 0 && posY < boardSize) {
        posX = Math.trunc(posX);
        posY = Math.trunc(posY);
        g_currentPiece = posX + posY * boardSize;
        replace_stones(g_board);
    }
});

putButton.addEventListener('click', function (event) {
    if (g_currentPiece !== -1 && g_history_last)
        gameSocket.emit("put", {usertoken: electron.remote.getGlobal('sharedObject').userToken, pos: g_currentPiece});
});

passButton.addEventListener('click', function (event) {
    if (g_history_last)
        gameSocket.emit("pass", {usertoken: electron.remote.getGlobal('sharedObject').userToken});
});

iaCheckBox.addEventListener('click', function (event) {
    if (iaSelectionButton.files.length === 0)
        iaCheckBox.checked = false;
    if (iaCheckBox.checked)
        gameActionContainer.style.display = "none";
    else
        gameActionContainer.style.display = "block";

    if (iaCheckBox.checked) {
        if ('files' in iaSelectionButton) {
            if (iaSelectionButton.files.length !== 0) {
                let file = iaSelectionButton.files[0];
                bot.launch(file.path);
                if (g_turn === electron.remote.getGlobal('sharedObject').pseudonyme)
                    bot.init();
            }
        }
    } else if (g_ia) {
        bot.stop();
    }
});

iaSelectionButton.addEventListener('change', function (event) {
    if ('files' in iaSelectionButton) {
        if (iaSelectionButton.files.length !== 0) {
            let file = iaSelectionButton.files[0];
            if (g_ia) {
                bot.stop();
                bot.launch(file.path);
                if (g_turn === electron.remote.getGlobal('sharedObject').pseudonyme)
                    bot.init();
            }
        }
    }
});

backButton.addEventListener('click', function (event) {
    document.location.replace(path.join(__dirname, 'res/home.html'));
});

sendMessageButton.addEventListener('click', function () {
    if (newMessageInput.value !== "")
        gameSocket.emit("message", {
            usertoken: electron.remote.getGlobal("sharedObject").userToken,
            msg: newMessageInput.value
        });
    newMessageInput.value = "";
});

activateTerritoryCheckBox.addEventListener('click', function () {
    if (!g_history_last)
        activateTerritoryCheckBox.checked = false;
    if (activateTerritoryCheckBox.checked) {
        territoryContainer.style.display = "block";
    } else {
        territoryContainer.style.display = "none";
    }
});

historyFirst.addEventListener('click', function () {
    g_current_history = 0;
    refresh_history();

});

historyPrev.addEventListener('click', function () {
    if (g_current_history > 0)
        g_current_history--;
    refresh_history();
});

historyNext.addEventListener('click', function () {
    if (g_current_history < g_history.length - 1)
        g_current_history++;
    refresh_history();
});

historyLast.addEventListener('click', function () {
    g_current_history = g_history.length - 1;
    refresh_history();
});


/*
 * ------------------------------
 * Main
 * ------------------------------
 */

g_isConnected = electron.remote.getGlobal('sharedObject').isConnected;
if (g_isConnected)
    gameSocket.emit('userJoin', {usertoken: electron.remote.getGlobal('sharedObject').userToken});
gameSocket.emit("getBoard");
gameSocket.emit("getInfo");
