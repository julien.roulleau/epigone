import { TokenGenerator } from "../utils/TokenGenerator";

export class User {
    username: string;
    token: string;
    rooms = new Map<string, undefined>();

    constructor(username: string) {
        this.username = username;
        this.token = TokenGenerator.gen();
    }

    join(name: string) {
        this.rooms.set(name, undefined);
    }

    leave(name: string) {
        return this.rooms.delete(name);
    }
}