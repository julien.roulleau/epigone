import { User } from "./User";

export class MUser {
    private static usersByUsername = new Map<string, User>();
    private static usersByToken = new Map<string, User>();

    static getUserByUsername(username: string) {
        return MUser.usersByUsername.get(username);
    }

    static getUserByToken(username: string) {
        return MUser.usersByToken.get(username);
    }

    static createUser(username: string) {
        if (!MUser.usersByUsername.has(username)) {
            const user = new User(username);
            MUser.usersByUsername.set(user.username, user);
            MUser.usersByToken.set(user.token, user);
            return user.token;
        } else {
            return undefined;
        }
    }

    static removeUser(token: string) {
        const user = MUser.usersByToken.get(token);
        if (user) {
            MUser.usersByUsername.delete(user.username);
            MUser.usersByToken.delete(user.token);
            return true;
        } else {
            return false;
        }
    }
}