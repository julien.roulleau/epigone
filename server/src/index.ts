import express from "express";
import login from "./api/login";
import room from "./api/room";
import http from "http";
import { Global } from "./utils/Global";
import socket from "socket.io";

const app = express();
const server = http.createServer(app);

Global.io = socket(server);

app.use("/login", login);
app.use("/room", room);

server.listen(8080, () => {
    console.log("Server listen on 8080");
});