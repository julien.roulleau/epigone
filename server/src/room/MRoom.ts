import { Room } from "./Room";
import { User } from "../user/User";

export class MRoom {
    private static rooms = new Map<string, Room>();

    static list() {
        const rooms: any[] = [];
        MRoom.rooms.forEach((value) => {
            rooms.push({
                name: value.name,
                board: value.board,
                canJoin: value.white === undefined || value.black === undefined,
                white: value.white ? value.white.username : undefined,
                black: value.black ? value.black.username : undefined
            });
        });
        return rooms;
    }

    static findRoomByName(name: string): Room {
        let ret: Room = undefined;
        MRoom.rooms.forEach((value) => {
            if (value.name === name) {
                ret = value;
                return false;
            }
        });
        return ret;
    }

    static create(user: User, name: string, size: number, timeout: number) {
        if (!MRoom.findRoomByName(name)) {
            const room = new Room(name, size, timeout);
            MRoom.rooms.set(name, room);
            if (room.join(user)) {
                return room;
            } else {
                return undefined;
            }
        } else {
            return undefined;
        }
    }

    static delete(name: string) {
        const room = MRoom.findRoomByName(name);
        if (room) {
            room.leave();
            MRoom.rooms.delete(name);
            return true;
        } else {
            return false;
        }
    }

    static join(name: string, user: User) {
        const room = MRoom.findRoomByName(name);
        if (room) {
            return room.join(user);
        } else {
            return false;
        }
    }

    static leave(name: string, user: User) {
        const room = MRoom.findRoomByName(name);
        if (room) {
            if (room.leave(user)) {
                if (room.black === undefined || room.white === undefined) {
                    return MRoom.delete(room.name);
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
