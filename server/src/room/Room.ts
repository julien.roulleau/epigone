import { User } from "../user/User";
import { Board } from "../board/Board";
import { Move, MoveType } from "../board/Move";

export class Room {
    name: string;
    board: Board;
    black: User = undefined;
    white: User = undefined;
    winner: User = undefined;
    isLock = false;
    history: Move[] = [];
    intervalID: NodeJS.Timeout;
    timeout: number;

    constructor(name: string, size: number, timeout: number) {
        this.name = name;
        this.board = new Board(size);
        this.timeout = timeout;
    }

    turn() {
        if (this.history.length > 0) {
            return this.history[this.history.length - 1].user === this.black.username ? this.white : this.black;
        } else {
            return this.black;
        }
    }

    join(user: User) {
        if (this.black === undefined && this.white != user) {
            this.black = user;
            user.join(this.name);
            return true;
        } else if (this.white === undefined && this.black != user) {
            this.white = user;
            user.join(this.name);
            return true;
        } else {
            return false;
        }
    }

    leave(user?: User) {
        if (user) {
            if (this.black == user) {
                this.black = undefined;
            } else if (this.white == user) {
                this.white = undefined;
            } else {
                return false;
            }
            return user.leave(this.name);
        } else {
            if (this.black) {
                this.black.leave(this.name);
                this.black = undefined;
            }
            if (this.white) {
                this.white.leave(this.name);
                this.white = undefined;
            }
            return true;
        }
    }

    put(user: User, cord: number) {
        if (this.turn() === user) {
            const move = this.board.doMove(cord, this.turn() === this.black ? 1 : 2);
            if (move !== undefined) {
                this.history.push({
                    user: user.username,
                    type: MoveType.PUT,
                    ...move
                });
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    pass(user: User) {
        if (this.turn() === user) {
            this.history.push({
                user: user.username,
                type: MoveType.PASS,
                add: undefined,
                remove: undefined,
            });
            return true;
        } else {
            return false;
        }
    }
}