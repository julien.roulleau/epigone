import { Pos } from "./Pos";

export enum BoardStatus {
    tmp = -1,
    empty = 0,
    black = 1,
    white = 2,
    territoryOffset = 10,
    blackTerritory = 11,
    whiteTerritory = 12,
    neutralTerritory = 13,
    blackDeath = 111,
    whiteDeath = 112,
}

export class Board {
    board: number[];
    size: number;
    blackPoint: number;
    whitePoint: number;

    constructor(size: number) {
        this.size = size;
        this.board = Array(size * size).fill(0);
        this.blackPoint = 0;
        this.whitePoint = 7.5;
    }

    doMove(cord: number, player: number) {
        let PiecesRemoved: number[] = [];
        if (this.moveIsValid(cord, player)) {
            this.board[cord] = player;
            const pos = this.cordToPos(cord);
            PiecesRemoved = PiecesRemoved.concat(this.removeBlockIfIsDead(this.posToCord(new Pos(pos.x + 1, pos.y))));
            PiecesRemoved = PiecesRemoved.concat(this.removeBlockIfIsDead(this.posToCord(new Pos(pos.x - 1, pos.y))));
            PiecesRemoved = PiecesRemoved.concat(this.removeBlockIfIsDead(this.posToCord(new Pos(pos.x, pos.y + 1))));
            PiecesRemoved = PiecesRemoved.concat(this.removeBlockIfIsDead(this.posToCord(new Pos(pos.x, pos.y - 1))));
            if (player === 1) {
                this.blackPoint += PiecesRemoved.length;
            } else {
                this.whitePoint += PiecesRemoved.length;
            }
            return {add: cord, remove: PiecesRemoved};
        } else {
            return undefined;
        }
    }

    private moveIsValid(cord: number, player: number) {
        if (this.board[cord] !== 0) {
            return false;
        }
        const enemy = player === BoardStatus.black ? BoardStatus.white : BoardStatus.black;
        const pos = this.cordToPos(cord);
        const checkIfIsPlayerAndIfBlockLife = (cord: number, player: number) => {
            return (this.board[cord] === player) ? this.countBlockLife(cord) : 0;
        };
        if (this.countBlockLife(cord) > 0
            || checkIfIsPlayerAndIfBlockLife(this.posToCord(new Pos(pos.x + 1, pos.y)), player) > 1
            || checkIfIsPlayerAndIfBlockLife(this.posToCord(new Pos(pos.x - 1, pos.y)), player) > 1
            || checkIfIsPlayerAndIfBlockLife(this.posToCord(new Pos(pos.x, pos.y + 1)), player) > 1
            || checkIfIsPlayerAndIfBlockLife(this.posToCord(new Pos(pos.x, pos.y - 1)), player) > 1) {
            return true;
        } else {
            return checkIfIsPlayerAndIfBlockLife(this.posToCord(new Pos(pos.x + 1, pos.y)), enemy) === 1
                || checkIfIsPlayerAndIfBlockLife(this.posToCord(new Pos(pos.x - 1, pos.y)), enemy) === 1
                || checkIfIsPlayerAndIfBlockLife(this.posToCord(new Pos(pos.x, pos.y + 1)), enemy) === 1
                || checkIfIsPlayerAndIfBlockLife(this.posToCord(new Pos(pos.x, pos.y - 1)), enemy) === 1;
        }
    }

    private cordToPos(cord: number) {
        return new Pos((cord % this.size), Math.floor(cord / this.size));
    }

    private posToCord(pos: Pos) {
        if (pos.y < 0 || pos.x < 0 || pos.y >= this.size || pos.x >= this.size) {
            return -1;
        } else {
            return pos.y * this.size + pos.x;
        }
    }

    private isOnBoard(cord: number) {
        return cord >= 0 && cord < this.size * this.size;
    }

    private countBlockLife(cord: number) {
        if (this.isOnBoard(cord)) {
            const board = [...this.board];
            const player = board[cord];
            board[cord] = BoardStatus.tmp;
            return this.countBlockLiveBis(board, cord, player, BoardStatus.empty);
        } else {
            return 0;
        }
    }

    private countBlockLiveBis(board: number[], cord: number, player: number, livePieces: number) {
        let lives = 0;
        const pos = this.cordToPos(cord);
        lives += this.countBlockLiveBisBis(board, this.posToCord(new Pos(pos.x + 1, pos.y)), player, livePieces);
        lives += this.countBlockLiveBisBis(board, this.posToCord(new Pos(pos.x - 1, pos.y)), player, livePieces);
        lives += this.countBlockLiveBisBis(board, this.posToCord(new Pos(pos.x, pos.y + 1)), player, livePieces);
        lives += this.countBlockLiveBisBis(board, this.posToCord(new Pos(pos.x, pos.y - 1)), player, livePieces);
        return lives;
    }

    private countBlockLiveBisBis(board: number[], cord: number, player: number, livePieces: number) {
        if (!this.isOnBoard(cord)) {
            return 0;
        } else if (board[cord] === livePieces) {
            board[cord] = BoardStatus.tmp;
            return 1;
        } else if (board[cord] === player) {
            board[cord] = BoardStatus.tmp;
            return this.countBlockLiveBis(board, cord, player, livePieces);
        } else {
            return 0;
        }
    }

    private removeBlockIfIsDead(cord: number): number[] {
        if (this.isOnBoard(cord) && this.board[cord] !== 0 && this.countBlockLife(cord) === 0) {
            return this.removeBlock(cord, this.board[cord]);
        }
        return [];
    }

    private removeBlock(cord: number, player: number) {
        let PiecesRemoved: number[] = [];
        if (this.isOnBoard(cord) && this.board[cord] === player) {
            PiecesRemoved.push(cord);
            const pos = this.cordToPos(cord);
            this.board[cord] = BoardStatus.empty;
            PiecesRemoved = PiecesRemoved.concat(this.removeBlock(this.posToCord(new Pos(pos.x + 1, pos.y)), player));
            PiecesRemoved = PiecesRemoved.concat(this.removeBlock(this.posToCord(new Pos(pos.x - 1, pos.y)), player));
            PiecesRemoved = PiecesRemoved.concat(this.removeBlock(this.posToCord(new Pos(pos.x, pos.y + 1)), player));
            PiecesRemoved = PiecesRemoved.concat(this.removeBlock(this.posToCord(new Pos(pos.x, pos.y - 1)), player));
        }
        return PiecesRemoved;
    }

    private setTerritory(board: number[]) {
        board.forEach((value, index) => {
            if (value === BoardStatus.empty) {
                this.setTerritoryBis(board, index, -1, BoardStatus.tmp);
            }
        });
    }

    private setTerritoryBis(board: number[], cord: number, lastCord: number, to: number) {
        if (this.isOnBoard(cord) && board[cord] !== BoardStatus.neutralTerritory && board[cord] !== to) {
            const pos = this.cordToPos(cord);
            if (board[cord] === BoardStatus.black || board[cord] === BoardStatus.white) {
                this.setTerritoryBis(board, lastCord, cord, board[cord] + BoardStatus.territoryOffset);
            } else if ((board[cord] === BoardStatus.blackTerritory || board[cord] === BoardStatus.whiteTerritory)
                && board[cord] !== to && to !== BoardStatus.tmp) {
                board[cord] = BoardStatus.neutralTerritory;
                this.setTerritoryBis(board, this.posToCord(new Pos(pos.x + 1, pos.y)), cord, BoardStatus.neutralTerritory);
                this.setTerritoryBis(board, this.posToCord(new Pos(pos.x - 1, pos.y)), cord, BoardStatus.neutralTerritory);
                this.setTerritoryBis(board, this.posToCord(new Pos(pos.x, pos.y + 1)), cord, BoardStatus.neutralTerritory);
                this.setTerritoryBis(board, this.posToCord(new Pos(pos.x, pos.y - 1)), cord, BoardStatus.neutralTerritory);
            } else if (board[cord] !== BoardStatus.blackTerritory && board[cord] !== BoardStatus.whiteTerritory) {
                board[cord] = to;
                this.setTerritoryBis(board, this.posToCord(new Pos(pos.x + 1, pos.y)), cord, to);
                this.setTerritoryBis(board, this.posToCord(new Pos(pos.x - 1, pos.y)), cord, to);
                this.setTerritoryBis(board, this.posToCord(new Pos(pos.x, pos.y + 1)), cord, to);
                this.setTerritoryBis(board, this.posToCord(new Pos(pos.x, pos.y - 1)), cord, to);
            }
        }
    }

    private removeGivenBlock(board: number[], cord: number, player: number) {
        let piecesRemoved: number[] = [];
        if (this.isOnBoard(cord) && board[cord] === player) {
            const pos = this.cordToPos(cord);
            piecesRemoved.push(cord);
            board[cord] = BoardStatus.empty;
            piecesRemoved = piecesRemoved.concat(this.removeGivenBlock(board, this.posToCord(new Pos(pos.x + 1, pos.y)), player));
            piecesRemoved = piecesRemoved.concat(this.removeGivenBlock(board, this.posToCord(new Pos(pos.x - 1, pos.y)), player));
            piecesRemoved = piecesRemoved.concat(this.removeGivenBlock(board, this.posToCord(new Pos(pos.x, pos.y + 1)), player));
            piecesRemoved = piecesRemoved.concat(this.removeGivenBlock(board, this.posToCord(new Pos(pos.x, pos.y - 1)), player));
        }
        return piecesRemoved;
    }

    private removeDeadBlock(board: number[]) {
        let totalPiecesRemoved: number[] = [];
        board.forEach((value, index) => {
            if (value === BoardStatus.black
                && this.countBlockLiveBis([...board], index, BoardStatus.black, BoardStatus.blackTerritory)
                + this.countBlockLiveBis([...board], index, BoardStatus.black, BoardStatus.neutralTerritory) < 2) {
                const piecesRemoved = this.removeGivenBlock(board, index, value);
                this.blackPoint += piecesRemoved.length;
                totalPiecesRemoved = totalPiecesRemoved.concat(piecesRemoved);
            } else if (value === BoardStatus.white
                && this.countBlockLiveBis([...board], index, BoardStatus.white, BoardStatus.whiteTerritory)
                + this.countBlockLiveBis([...board], index, BoardStatus.white, BoardStatus.neutralTerritory) < 2) {
                const piecesRemoved = this.removeGivenBlock(board, index, value);
                this.whitePoint += piecesRemoved.length;
                totalPiecesRemoved = totalPiecesRemoved.concat(piecesRemoved);
            }
        });
        return totalPiecesRemoved;
    }

    private cleanSpecialStatusOnBoard(board: number[]) {
        board.forEach((value, index) => {
            if (value === BoardStatus.neutralTerritory
                || value === BoardStatus.blackTerritory
                || value === BoardStatus.whiteTerritory
                || value === BoardStatus.tmp) {
                board[index] = BoardStatus.empty;
            }
        });
    }

    boardStatus() {
        const territoryBoard = this.computeTerritoryBoard();
        territoryBoard.territoryBoard.forEach((value, index) => {
            if (this.board[index] === BoardStatus.black
                && value === BoardStatus.whiteTerritory) {
                territoryBoard.territoryBoard[index] = BoardStatus.blackDeath;
            } else if (this.board[index] === BoardStatus.white
                && value === BoardStatus.blackTerritory) {
                territoryBoard.territoryBoard[index] = BoardStatus.whiteDeath;
            } else if (value === BoardStatus.tmp) {
                territoryBoard.territoryBoard[index] = BoardStatus.neutralTerritory;
            }
        });
        return territoryBoard;
    }

    computeTerritoryBoard() {
        const board = [...this.board];
        this.setTerritory(board);
        const piecesRemoved = this.removeDeadBlock(board);
        this.cleanSpecialStatusOnBoard(board);
        this.setTerritory(board);
        return {territoryBoard: board, piecesRemove: piecesRemoved};
    }

    countPlayerPoint(board: number[], player: number) {
        const point = board.reduce((accumulator, currentValue) => {
            return currentValue === BoardStatus.territoryOffset + player ? accumulator + 1 : accumulator;
        }) - board[0];
        return point + (player === BoardStatus.black ? this.blackPoint : this.whitePoint);
    }
}