export enum MoveType {
    PUT = "PUT",
    PASS = "PASS",
    END = "END",
}

export interface Move {
    user: string;
    type: MoveType;
    add: number;
    remove: number[];
}