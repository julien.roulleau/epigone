export class TokenGenerator {
    private static tokens = new Map<string, undefined>();

    static gen() {
        while (true) {
            const token = Math.random().toString(36).substr(2, 9);
            if (!TokenGenerator.tokens.has(token)) {
                TokenGenerator.tokens.set(token, undefined);
                return token;
            }
        }
    }
}