import express from "express";
import { MRoom } from "../room/MRoom";
import { MUser } from "../user/MUser";
import { game } from "../socket/game";

const router = express.Router();

router.get("/list", function (req, res) {
    res.statusCode = 200;
    res.json({
        data: {
            games: MRoom.list()
        }
    });
});

router.get("/create", function (req, res) {
    const roomname = req.headers.roomname;
    const userToken = req.headers.usertoken;
    const size = req.headers.size;
    const timeout = req.headers.timeout;
    if (typeof userToken != "string" || typeof roomname != "string" || typeof size != "string" || typeof timeout != "string") {
        res.statusCode = 500;
        res.json({error: {message: "Bad request."}});
        return;
    }
    const user = MUser.getUserByToken(userToken);
    if (!user) {
        res.statusCode = 500;
        res.json({error: {message: "Invalid token."}});
        return;
    }

    const room = MRoom.create(user, roomname, parseInt(size), parseInt(timeout) * 1000);
    if (room) {
        game(roomname, room);
        res.sendStatus(200);
    } else {
        res.statusCode = 500;
        res.json({error: {message: "Room name already used."}});
    }
});

router.get("/join", function (req, res) {
    const roomname = req.headers.roomname;
    const userToken = req.headers.usertoken;
    if (typeof userToken != "string" || typeof roomname != "string") {
        res.statusCode = 500;
        res.json({error: {message: "Bad request."}});
        return;
    }
    const user = MUser.getUserByToken(userToken);
    if (!user) {
        res.statusCode = 500;
        res.json({error: {message: "Invalid token."}});
        return;
    }
    if (MRoom.join(roomname, user)) {
        res.sendStatus(200);
    } else {
        res.statusCode = 500;
        res.json({error: {message: "You cannot join."}});
    }
});

router.get("/leave", function (req, res) {
    const roomname = req.headers.roomname;
    const userToken = req.headers.usertoken;
    if (typeof userToken != "string" || typeof roomname != "string") {
        res.statusCode = 500;
        res.json({error: {message: "Bad request."}});
        return;
    }
    const user = MUser.getUserByToken(userToken);
    if (!user) {
        res.statusCode = 500;
        res.json({error: {message: "Invalid token."}});
        return;
    }
    if (MRoom.leave(roomname, user)) {
        res.sendStatus(200);
    } else {
        res.statusCode = 500;
        res.json({error: {message: "You cannot leave."}});
    }
});

export default router;