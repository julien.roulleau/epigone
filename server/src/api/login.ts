import express from "express";
import { MUser } from "../user/MUser";

const router = express.Router();

router.get("/login", function (req, res) {
    const username = req.headers.username;
    if (typeof username === "string") {
        const userToken = MUser.createUser(username);
        if (userToken) {
            res.statusCode = 200;
            res.json({data: {usertoken: userToken}});
        } else {
            res.statusCode = 500;
            res.json({error: {message: "username already used"}});
        }
    } else {
        res.statusCode = 500;
        res.json({error: {message: "Bad request."}});
    }
});

router.get("/logout", function (req, res) {
    const userToken = req.headers.usertoken;
    if (typeof userToken === "string") {
        if (MUser.removeUser(userToken)) {
            res.sendStatus(200);
        } else {
            res.statusCode = 500;
            res.json({error: {message: "Invalid token"}});
        }
    } else {
        res.statusCode = 500;
        res.json({error: {message: "Bad request."}});
    }
});

export default router;