import { MUser } from "../user/MUser";
import { Global } from "../utils/Global";
import { Room } from "../room/Room";
import { Move, MoveType } from "../board/Move";

function execPass(room: Room) {
    if (room.history.length >= 2 && room.history[room.history.length - 2].type === MoveType.PASS) {
        clearInterval(room.intervalID);
        const territoryBoard = room.board.computeTerritoryBoard();
        const move: Move = {
            user: "EpiGone",
            type: MoveType.END,
            add: undefined,
            remove: territoryBoard.piecesRemove,
        };
        room.history.push(move);
        room.board.board = territoryBoard.territoryBoard;
        room.board.blackPoint = room.board.countPlayerPoint(room.board.board, 1);
        room.board.whitePoint = room.board.countPlayerPoint(room.board.board, 2);
        room.winner = room.board.blackPoint > room.board.whitePoint ? room.black : room.white;
        room.isLock = true;
        const ret = {
            data: {
                board: room.board.board,
                endMove: move,
                blackPoint: room.board.blackPoint,
                whitePoint: room.board.whitePoint,
                winner: room.winner.username,
            }
        };
        this.emit("end", ret);
        this.broadcast.emit("end", ret);
    } else {
        const ret = {
            data: room.history[room.history.length - 1]
        };
        this.emit("pass", ret);
        this.broadcast.emit("pass", ret);
    }
}

function setTurnTimeout(room: Room) {
    if (room.timeout !== 0 && room.black !== undefined && room.white !== undefined) {
        clearInterval(room.intervalID);
        room.intervalID = setInterval(() => {
            const move: Move = {
                user: room.turn().username,
                type: MoveType.PASS,
                add: undefined,
                remove: undefined,
            };
            room.history.push(move);
            execPass.bind(this)(room);
        }, room.timeout);
    }
}

export function game(path: string, room: Room) {
    Global.io.of(path).on("connect", (connection) => {
        connection.on("userJoin", function (data: any) {
            if (typeof data === "object"
                && typeof data.usertoken === "string") {
                const user = MUser.getUserByToken(data.usertoken);
                if (user) {
                    const ret = {
                        data: {
                            user: user.username,
                        }
                    };
                    this.broadcast.emit("userJoin", ret);
                } else {
                    this.emit("userJoin", {error: {message: "Invalid token."}});
                }
            }
        });

        connection.on("message", function (data: any) {
            if (typeof data === "object"
                && typeof data.usertoken === "string"
                && typeof data.msg === "string") {
                const user = MUser.getUserByToken(data.usertoken);
                if (user) {
                    const ret = {
                        data: {
                            user: user.username,
                            msg: data.msg
                        }
                    };
                    this.emit("message", ret);
                    this.broadcast.emit("message", ret);
                } else {
                    this.emit("message", {error: {message: "Invalid token."}});
                }
            }
        });

        connection.on("getBoard", function () {
            this.emit("getBoard", {data: {board: room.board}});
        });

        connection.on("getInfo", function () {
            this.emit("getInfo", {
                data: {
                    turn: room.turn() ? room.turn().username : undefined,
                    isFinish: room.isLock,
                    boardSize: room.board.size,
                    black: room.black ? room.black.username : undefined,
                    white: room.white ? room.white.username : undefined,
                    winner: room.winner,
                    blackPoint: room.board.blackPoint,
                    whitePoint: room.board.whitePoint,
                    timeout: room.timeout
                }
            });
        });

        connection.on("getHistory", function () {
            this.emit("getHistory", {data: room.history});
        });

        connection.on("getTerritoryBoard", function () {
            this.emit("getTerritoryBoard", {data: room.board.boardStatus()});
        });

        connection.on("put", function (data: any) {
            if (!room.isLock
                && typeof data === "object"
                && typeof data.usertoken === "string"
                && typeof data.pos === "number") {
                const user = MUser.getUserByToken(data.usertoken);
                if (user) {
                    if (room.black == user || room.white == user) {
                        if (room.put(user, data.pos)) {
                            setTurnTimeout.bind(this)(room);
                            const ret = {
                                data: room.history[room.history.length - 1]
                            };
                            this.emit("put", ret);
                            this.broadcast.emit("put", ret);
                        } else {
                            this.emit("put", {error: {message: "You can't do that."}});
                        }
                    } else {
                        this.emit("put", {error: {message: "Your not a player."}});
                    }
                } else {
                    this.emit("put", {error: {message: "Invalid token."}});
                }
            } else {
                this.emit("put", {error: {message: "Bad request."}});
            }
        });

        connection.on("pass", function (data: any) {
            if (!room.isLock
                && typeof data === "object"
                && typeof data.usertoken === "string") {
                const user = MUser.getUserByToken(data.usertoken);
                if (user) {
                    if (room.black == user || room.white == user) {
                        if (room.pass(user)) {
                            setTurnTimeout.bind(this)(room);
                            execPass.bind(this)(room);
                        } else {
                            this.emit("pass", {error: {message: "You can't do that."}});
                        }
                    } else {
                        this.emit("pass", {error: {message: "Your not a player."}});
                    }
                } else {
                    this.emit("pass", {error: {message: "Invalid token."}});
                }
            } else {
                this.emit("pass", {error: {message: "Bad request."}});
            }
        });
    });
}
